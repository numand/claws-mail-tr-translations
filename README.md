This repository contains Turkish translations of [Claws Mail] and its [plugins].

[Claws Mail]: http://www.claws-mail.org/
[plugins]: http://www.claws-mail.org/plugins.php?section=downloads